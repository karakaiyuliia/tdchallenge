package com.ykarakai.cofund;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ykarakai.cofund.database.Transactions;
import com.ykarakai.cofund.ui.main.Details;

import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    List<Transactions> transacList;

    public Adapter(List<Transactions> transacList, Context context) {
        this.transacList = transacList;
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_row,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(Adapter.ViewHolder holder, int position) {
        switch (Details.categories.get(transacList.get(position).getCategoryId() - 1)) {
            case "Grocery":
                holder.image.setImageResource(R.drawable.grocery);
                break;
            case "Pet":
                holder.image.setImageResource(R.drawable.pet);
                break;
            case "Bills":
                holder.image.setImageResource(R.drawable.bills);
                break;
            case "Education":
                holder.image.setImageResource(R.drawable.education);
                break;
            case "Entertainment":
                holder.image.setImageResource(R.drawable.entertainment);
                break;
            case "Gifts":
                holder.image.setImageResource(R.drawable.gift);
                break;
            case "Sports":
                holder.image.setImageResource(R.drawable.sport);
                break;
            case "Health":
                holder.image.setImageResource(R.drawable.health);
                break;
            case "Shopping":
                holder.image.setImageResource(R.drawable.shopping);
                break;
            case "Kids":
                holder.image.setImageResource(R.drawable.kids);
                break;
            case "Travel":
                holder.image.setImageResource(R.drawable.travel);
                break;
            case "Subscriptions":
                holder.image.setImageResource(R.drawable.subscription);
                break;
            case "Auto & Transport":
                holder.image.setImageResource(R.drawable.transport);
                break;
            case "Other":
                holder.image.setImageResource(R.drawable.other);
                break;
        }
        holder.text.setText(transacList.get(position).getDetails() + " - $" + transacList.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return transacList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{

        ImageView image;
        TextView text;

        ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iconId);
            text = itemView.findViewById(R.id.transactionId);
            itemView.setOnLongClickListener(this);
        }


        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }
}