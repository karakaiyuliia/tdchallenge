package com.ykarakai.cofund.database;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class DbViewModel extends AndroidViewModel {

    private Repository mRepository;
    private LiveData<List<Category>> mAllCategories;
    private LiveData<List<Dates>> mAllMonthlyData;

    public DbViewModel(@NonNull Application application) {
        super(application);
        mRepository = new Repository(application);
        mAllMonthlyData = mRepository.getAllMonthlyData();
        mAllCategories = mRepository.getAllCategories();
    }

    public LiveData<List<Transactions>> getTransactionsByDateId(int dateId) {
        return mRepository.getTransactionsByDateId(dateId);
    }

    public LiveData<List<Category>> getAllCategories() {
        return mAllCategories;
    }

    public LiveData<List<Dates>> getAllMonthlyData() {
        return mAllMonthlyData;
    }

    public void delete(Transactions transaction, String date){mRepository.delete(transaction, date);}
    public void insert(Dates dates) {
        mRepository.insert(dates);
    }
    public void insert(Transactions transaction) {
        mRepository.insert(transaction);
    }
    public void updateBudget(String date, int budget){mRepository.updateBudget(date, budget);}
    public void updateSpent(String date, double spent){mRepository.updateSpent(date, spent);}
    public double getSpent(String date){return mRepository.getSpent(date);}
    public int getBudget(String date){return mRepository.getBudget(date);}
}
