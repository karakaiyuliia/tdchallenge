package com.ykarakai.cofund.database;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;

public class Repository {

    private DAO mDao;
    private LiveData<List<Dates>> mAllMonthlyData;
    private LiveData<List<Category>> mAllcategories;
    private Database db;

    Repository(Application application) {
        db = Database.getDatabase(application);
        mDao = db.dao();
        mAllMonthlyData = mDao.getAllMonthlyData();
        mAllcategories = mDao.getAllCategories();
    }

    LiveData<List<Transactions>> getTransactionsByDateId(int dateId) {
        return mDao.getTransactionsByDateId(dateId);
    }
    LiveData<List<Dates>> getAllMonthlyData() { return mAllMonthlyData; }
    LiveData<List<Category>> getAllCategories() { return mAllcategories; }

    double getSpent(String date){return mDao.getSpent(date);}
    int getBudget(String date){return mDao.getBudget(date);}
    void delete(Transactions transaction, String date){ new deleteAsyncTransactions(mDao).execute(transaction, date); }
    void insert(Dates dates) {
        new insertAsyncDates(mDao).execute(dates);
    }
    void insert(Transactions transaction) {
         new insertAsyncTransaction(mDao).execute(transaction);
    }
    void updateBudget(String date, int budget){new updateBudgetAsyncTask(mDao).execute(date, budget);}
    void updateSpent(String date, double spent){new updateSpentAsyncTask(mDao).execute(date, spent);}

    private static class deleteAsyncTransactions extends AsyncTask<Object, Void, Void> {

        private DAO mAsyncTaskDao;

        deleteAsyncTransactions(DAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... params) {
            double currentSpent = mAsyncTaskDao.getSpent((String)params[1]);
            Transactions tr = (Transactions) params[0];
            mAsyncTaskDao.updateSpent((String)params[1], currentSpent - tr.getAmount());
            mAsyncTaskDao.delete((Transactions) params[0]);
            return null;
        }
    }

    private static class insertAsyncDates extends AsyncTask<Dates, Void, Void> {

        private DAO mAsyncTaskDao;

        insertAsyncDates(DAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Dates... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class insertAsyncTransaction extends AsyncTask<Transactions, Void, Void> {

        private DAO mAsyncTaskDao;

        insertAsyncTransaction(DAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Transactions... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateBudgetAsyncTask extends AsyncTask<Object, Void, Void>{
        private DAO mAsyncTaskDao;

        updateBudgetAsyncTask(DAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... params) {
            mAsyncTaskDao.updateBudget((String)params[0], (int) params[1]);
            return null;
        }

    }

    private static class updateSpentAsyncTask extends AsyncTask<Object, Void, Void>{
        private DAO mAsyncTaskDao;

        updateSpentAsyncTask(DAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... params) {
            double currentSpent = mAsyncTaskDao.getSpent((String)params[0]);
            mAsyncTaskDao.updateSpent((String)params[0], (double)params[1] + currentSpent);
            return null;
        }

    }

}
