package com.ykarakai.cofund.database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface DAO {

    @Query("SELECT * from transactions_table where dates_id = :dateId")
    LiveData<List<Transactions>> getTransactionsByDateId(int dateId);

    @Query("SELECT * from dates_table")
    LiveData<List<Dates>> getAllMonthlyData();

    @Query("SELECT * from category_table")
    LiveData<List<Category>> getAllCategories();

    @Query("SELECT spent from dates_table where monthYear = :date")
    double getSpent(String date);

    @Query("SELECT budget from dates_table where monthYear = :date")
    int getBudget(String date);

    @Query("SELECT COUNT(*) FROM category_table")
    int categoryCount();

    @Insert()
    void insert(Transactions trans);

    @Insert()
    void insert(Dates dates);

    @Delete
    void delete(Transactions... transaction);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Category... category);

    @Query("UPDATE dates_table SET budget = :budget WHERE monthYear == :date")
    void updateBudget(String date, int budget);

    @Query("UPDATE dates_table SET spent = :spent WHERE monthYear == :date")
    void updateSpent(String date, double spent);
}
