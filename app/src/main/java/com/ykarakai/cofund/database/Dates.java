package com.ykarakai.cofund.database;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "dates_table")
public class Dates {
    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "monthYear")
    private String monthYear;

    @NonNull
    @ColumnInfo(name = "budget")
    private int budget;

    @NonNull
    @ColumnInfo(name = "spent")
    private double spent;

    public Dates(@NonNull String monthYear, @NonNull int budget, @NonNull double spent) {
        this.monthYear = monthYear; this.budget = budget; this.spent = spent;
    }

    @NonNull
    public String getMonthYear() {
        return this.monthYear;
    }

    @NonNull
    public int getId() {
        return this.id;
    }

    @NonNull
    public int getBudget() {
        return this.budget;
    }

    @NonNull
    public double getSpent() {
        return this.spent;
    }

}
