package com.ykarakai.cofund.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@androidx.room.Database(entities = {Dates.class, Transactions.class, Category.class}, version = 5)
public abstract class Database extends RoomDatabase {
    public abstract DAO dao();

    //db instance must have atomic access
    private static volatile Database INSTANCE;

    static Database getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (Database.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            Database.class, "database")
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    db.execSQL("INSERT INTO category_table (name) VALUES('Grocery'), ('Pet'), ('Education'), ('Entertainment'), ('Sports'), " +
                                            "('Health'), ('Gifts'), ('Shopping'), ('Kids'), ('Travel'), ('Bills'), ('Subscriptions'), ('Auto & Transport'), ('Other');");
                                }
                            })
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
