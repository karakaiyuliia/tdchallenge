package com.ykarakai.cofund.database;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "transactions_table", foreignKeys = {@ForeignKey(entity = Category.class,
        parentColumns = "id",
        childColumns = "category_id"), @ForeignKey(entity = Dates.class,
        parentColumns = "id",
        childColumns = "dates_id")})
public class Transactions {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "category_id")
    private int categoryId;

    @NonNull
    @ColumnInfo(name = "dates_id")
    private int dateId;

    @NonNull
    @ColumnInfo(name = "details")
    private String details;

    @NonNull
    @ColumnInfo(name = "amount")
    private double amount;

    public Transactions(@NonNull String details, @NonNull int categoryId, @NonNull int dateId, @NotNull double amount) {
        this.details = details;
        this.categoryId = categoryId;
        this.dateId = dateId;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }


    public void setAmount(double amount) {
        this.amount = amount;
    }

    @NonNull
    public String getDetails() {
        return this.details;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public int getCategoryId() {
        return categoryId;
    }

    @NonNull
    public int getDateId() {
        return dateId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCategoryId(@NonNull int categoryId) {
        this.categoryId = categoryId;
    }

    public void setDateId(@NonNull int dateId) {
        this.dateId = dateId;
    }

    public void setDetails(@NonNull String details) {
        this.details = details;
    }
}
