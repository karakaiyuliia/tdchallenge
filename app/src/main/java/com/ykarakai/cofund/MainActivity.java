package com.ykarakai.cofund;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.ykarakai.cofund.database.Dates;
import com.ykarakai.cofund.database.DbViewModel;
import com.ykarakai.cofund.ui.login.LoginActivity;
import com.ykarakai.cofund.ui.main.Chat;
import com.ykarakai.cofund.ui.main.Details;
import com.ykarakai.cofund.ui.main.SettingsBottomDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    public static DbViewModel mViewModel;
    private EditText budget;
    private TextView spent;
    private Spinner monthSpinner;
    private FloatingActionButton detailsBtn;
    private FloatingActionButton bot;
    private FloatingActionButton settings;
    private ProgressBar daysBar;
    private TextView daysLeft;
    private TextView dayslefttext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getSupportActionBar().hide();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));

//        Stetho.initializeWithDefaults(this);
//        new OkHttpClient.Builder()
//                .addNetworkInterceptor(new StethoInterceptor())
//                .build();

        daysLeft = findViewById(R.id.daysProgress);
        dayslefttext = findViewById(R.id.textProgress);

        int difference = getDaysDifference();
        daysLeft.setText(String.valueOf(difference));
        dayslefttext.setText(getResources().getString(R.string.days_left_txt));

        daysBar = findViewById(R.id.daysLeft);
        setProgressBar(calcProgress(difference));

        monthSpinner = findViewById(R.id.months);
        detailsBtn = findViewById(R.id.details);
        bot = findViewById(R.id.bot);
        settings = findViewById(R.id.settings);
        detailsBtn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.white)));
        bot.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.white)));
        settings.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.white)));
        budget = findViewById(R.id.budget);
        budget.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        spent = findViewById(R.id.spent);
        setListeners();

        mViewModel = new ViewModelProvider(this).get(DbViewModel.class);

        mViewModel.getAllMonthlyData().observe(this, new Observer<List<Dates>>() {
            @Override
            public void onChanged(@Nullable final List<Dates> data) {
                //no data in the db, first app launch
                if (data.size() == 0) {
                    Calendar c = Calendar.getInstance();
                    List<String> months = new ArrayList<String>();
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM");
                    int year = c.get(Calendar.YEAR);
                    String month = formatter.format(c.getTime());
                    months.add(month + " " + year);
                    populateMonths(months);

                    Dates dates = new Dates(month + " " + year, 0, 0);
                    mViewModel.insert(dates);
                } else {    //something is in db
                    List<String> months = new ArrayList<String>();

                    for (Dates d : data) {
                        months.add(d.getMonthYear());
                    }

                    //check is current month is new month
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM");
                    int year = c.get(Calendar.YEAR);
                    String month = formatter.format(c.getTime());
                    if(!months.contains(month + " " + year)){
                        months.add(month + " " + year);
                        Dates dates = new Dates(month + " " + year, 0, 0);
                        mViewModel.insert(dates);
                        spent.setText("0");

                    }else{
                        spent.setText(String.format("%.2f", data.get(data.size() - 1).getSpent()));
                    }

                    populateMonths(months);
                    if(data.get(data.size() - 1).getBudget() > 0)
                        budget.setText(String.valueOf(data.get(data.size() - 1).getBudget()));
                }
            }
        });

    }

    //calculate days difference for days left progress circle
    private int getDaysDifference(){
        Calendar cal = Calendar.getInstance();
        int res = cal.getActualMaximum(Calendar.DATE);
        return res - cal.get(Calendar.DAY_OF_MONTH);
    }

    private void setProgressBar(int progress){
        daysBar.setProgress(progress);
    }

    //for setting progress circle
    private int calcProgress(int diff){
        if(diff == 15){
            return 50;
        }else if(diff > 15 && diff <= 25){
            return 75;
        }else if(diff > 25 && diff < 28){
            return 85;
        }else if(diff == 29){
            return 95;
        }else if(diff == 30 || diff == 31){
            return 99;
        }else if(diff < 15 && diff >= 10){
            return 35;
        }else if(diff < 10 && diff >= 5){
            return 20;
        }else if(diff == 4){
            return 15;
        }else if(diff == 3){
            return 10;
        }else if(diff == 2){
            return 5;
        }else if(diff == 1){
            return 2;
        }else{
            return 0;
        }
    }

    //updating budget
    private void setListeners(){
        budget.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    try{
                        mViewModel.updateBudget(monthSpinner.getSelectedItem().toString(),  Integer.parseInt(budget.getText().toString()));
                    }catch(Exception ex){
                        Toast.makeText(getApplicationContext(), getString(R.string.parsing_exception), Toast.LENGTH_SHORT).show();
                    }
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert in != null;
                    in.hideSoftInputFromWindow(budget.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                    budget.clearFocus();
                    return true;
                }
                return false;
            }
        });
    }

    //populate spinner with provided month
    private void populateMonths(List<String> data){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,data);

        monthSpinner.setAdapter(adapter);
        monthSpinner.setSelection(data.size()-1);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //new thread, because this is a db operation
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int budgetRetrieved = mViewModel.getBudget(parent.getItemAtPosition(position).toString());
                        double spentRetrieved = mViewModel.getSpent(parent.getItemAtPosition(position).toString());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(budgetRetrieved > 0)
                                        budget.setText(Integer.toString(budgetRetrieved));
                                    spent.setText(String.format("%.2f",spentRetrieved));
                                }
                            });

                    }
                }).start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onTransactionsClicked(View view) {
        Intent intent = new Intent(this, Details.class);
        intent.putExtra("date", (int)monthSpinner.getSelectedItemId()+1);
        intent.putExtra("dateName", monthSpinner.getSelectedItem().toString());
        startActivity(intent);
    }

    public void onSettingsClicked(View view) {
        SettingsBottomDialog bottomSheet = new SettingsBottomDialog();
        bottomSheet.show(getSupportFragmentManager(), "settingsBottomSheet");
    }

    public void onChatbotClicked(View view) {
        Intent intent = new Intent(this, Chat.class);
        intent.putExtra("spent", spent.getText() == null ? "0" : spent.getText());
        intent.putExtra("budget", budget.getText().toString());
        startActivity(intent);
    }

    public void onLogoutClicked(View view) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.signout_dialog_name)
                .setMessage(R.string.signout_dialog_message)

                .setPositiveButton(R.string.signout_dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        logout();
                    }

                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    //delete db and logout
    private void logout(){
        getApplicationContext().deleteDatabase("database");
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
}
