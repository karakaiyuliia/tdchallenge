package com.ykarakai.cofund.ui.main;

import android.content.Context;
import android.content.res.Resources;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ykarakai.cofund.R;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class OnboardingPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    public int[] slide_images={R.drawable.budget, R.drawable.chatbot, R.drawable.delete};
    public String[] names;



    public OnboardingPagerAdapter(Context context, String[] names){
        this.context = context;
        this.names = names;
    }


    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImage = (ImageView) view.findViewById(R.id.pic);
        slideImage.getLayoutParams().height = 500;
        slideImage.getLayoutParams().width = 700;
        TextView slideText = (TextView) view.findViewById(R.id.heading);

        slideImage.setImageResource(slide_images[position]);
        slideText.setText(names[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout)object);
    }
}
