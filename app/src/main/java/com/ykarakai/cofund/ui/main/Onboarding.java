package com.ykarakai.cofund.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.R;

public class Onboarding extends AppCompatActivity {

    private ViewPager mSliderViewPager;
    private LinearLayout mDotsLayout;
    private OnboardingPagerAdapter mPagerAdapter;
    private TextView[] dots;
    private Button mPrevBtn;
    private Button mNextBtn;
    private int mCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getSupportActionBar().hide();
        setContentView(R.layout.activity_onboarding);

        mPrevBtn = findViewById(R.id.prev);
        mNextBtn = findViewById(R.id.next);

        String[] names = {getResources().getString(R.string.instr_budget), getResources().getString(R.string.instr_chat), getResources().getString(R.string.instr_delete)};

        mSliderViewPager = (ViewPager) findViewById(R.id.sliderViewPager);
        mDotsLayout = (LinearLayout) findViewById(R.id.bottomDots);
        mPagerAdapter = new OnboardingPagerAdapter(this, names);
        mSliderViewPager.setAdapter(mPagerAdapter);
        addDotsIndicator(0);
        mSliderViewPager.addOnPageChangeListener(viewListener);

        mNextBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(mCurrentPage == dots.length-1){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else{
                    mSliderViewPager.setCurrentItem(mCurrentPage + 1);
                }
            }
        });

        mPrevBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mSliderViewPager.setCurrentItem(mCurrentPage - 1);
            }
        });
    }

    public void addDotsIndicator(int position){
        dots = new TextView[3];
        mDotsLayout.removeAllViews();

        for(int i = 0; i < dots.length; ++i){
            dots[i] = new TextView(this);
            dots[i].setText(HtmlCompat.fromHtml("&#8226;", HtmlCompat.FROM_HTML_MODE_LEGACY));
            dots[i].setTextColor(getResources().getColor(R.color.colorAccentDisabled));
            dots[i].setTextSize(16);

            mDotsLayout.addView(dots[i]);
        }

        if(dots.length > 0){
            dots[position].setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
            mCurrentPage = position;

            if(position == 0){
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(false);
                mPrevBtn.setVisibility(View.INVISIBLE);

                mNextBtn.setText(R.string.next_btn);
                mPrevBtn.setText("");
            }else if(position == dots.length-1){
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(true);
                mPrevBtn.setVisibility(View.VISIBLE);

                mNextBtn.setText(R.string.finish_btn);
                mPrevBtn.setText(R.string.back_btn);
            }else{
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(true);
                mPrevBtn.setVisibility(View.VISIBLE);

                mNextBtn.setText(R.string.next_btn);
                mPrevBtn.setText(R.string.back_btn);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
