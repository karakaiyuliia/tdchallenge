package com.ykarakai.cofund.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.R;
import com.ykarakai.cofund.database.Transactions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

public class ChartFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;
    private static PieChart pieChart;

    public ChartFragment newInstance(int index) {
        ChartFragment fragment = new ChartFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);

        //room doesn't like data being restored in the Details for this fragment :/
        MainActivity.mViewModel.getTransactionsByDateId(Details.dateId).observe(this, data -> {
            buildChart();
        });
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chart, container, false);
        pieChart = root.findViewById(R.id.pieChart);
        buildChart();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        buildChart();
    }

    //buil the chart on the screen
    public static void buildChart(){

        if(Details.transacs.size() > 0){
            Map<String,Float> hm =
                    new HashMap<String,Float>();

            for (Transactions t : Details.transacs) {
                if(hm.containsKey(Details.categories.get(t.getCategoryId()-1))){
                    hm.put(Details.categories.get(t.getCategoryId()-1), hm.get(Details.categories.get(t.getCategoryId()-1) + t.getAmount()));
                }else{
                    hm.put(Details.categories.get(t.getCategoryId()-1), (float)t.getAmount());
                }
            }

            pieChart.setUsePercentValues(false);
            pieChart.setHoleRadius(0f);
            pieChart.setTransparentCircleRadius(0f);

            List<PieEntry> values = new ArrayList<>();

            for (Map.Entry<String, Float> entry : hm.entrySet()) {
                values.add(new PieEntry(entry.getValue(), entry.getKey()));
            }

            Description desc = new Description();
            desc.setText("");
            pieChart.setDescription(desc);

            Legend legend = pieChart.getLegend();
            legend.setEnabled(false);

            PieDataSet pieDataSet = new PieDataSet(values, "");
            PieData pieData = new PieData(pieDataSet);

            pieChart.setData(pieData);
            pieDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        }

    }
}