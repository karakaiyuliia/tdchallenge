package com.ykarakai.cofund.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.R;
import com.ykarakai.cofund.ui.login.LoginActivity;

//this is here just to determine whether the user is logged in
public class SplashActivity extends AppCompatActivity {
    public static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try
        {
            this.getSupportActionBar().hide();
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), R.color.gradientPink, null));
        }
        catch (NullPointerException e){}

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent intent = new Intent(this, MainActivity.class);
            //setAnimation();
            startActivity(intent);
            this.finish();
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            //setAnimation();
            startActivity(intent);
            this.finish();
        }
    }
}
