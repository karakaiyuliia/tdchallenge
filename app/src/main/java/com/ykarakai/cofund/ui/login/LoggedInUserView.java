package com.ykarakai.cofund.ui.login;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String displayName;
    private static boolean login;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(String displayName, boolean login) {
        this.displayName = displayName;
        this.login = login;
    }

    String getDisplayName() {
        return displayName;
    }

    public static boolean isLogin() {
        return login;
    }
}
