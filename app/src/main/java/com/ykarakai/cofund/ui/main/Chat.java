package com.ykarakai.cofund.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.Message;
import com.ykarakai.cofund.MessageAdapter;
import com.ykarakai.cofund.R;
import com.ykarakai.cofund.TextClassificationClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Chat extends AppCompatActivity {
    private TextClassificationClient client;
    private EditText inputEditText;
    private Handler handler;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Message> messageList = new ArrayList<Message>();
    private String spent;
    private String budget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle(R.string.chat_activity_name);

        Intent intent = getIntent();
        spent = intent.getStringExtra("spent");
        budget = intent.getStringExtra("budget");

        if(budget == null || budget.equals("")){
            budget = "0";
        }

        client = new TextClassificationClient(getApplicationContext());
        handler = new Handler();
        FloatingActionButton classifyButton = findViewById(R.id.logoutBtn);
        classifyButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        classifyButton.setOnClickListener(
                (View v) -> {
                    classify(inputEditText.getText().toString());
                });

        recyclerView = (RecyclerView) findViewById(R.id.messages);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new MessageAdapter(getBaseContext(), messageList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        inputEditText = findViewById(R.id.input_text);
    }

    //go back home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //loading text classifir client
    @Override
    protected void onStart() {
        super.onStart();
        handler.post(
                () -> {
                    client.load();
                });
    }

    //removing text classifier client
    @Override
    protected void onStop() {
        super.onStop();
        handler.post(
                () -> {
                    client.unload();
                });
    }

    /** Send input text to TextClassificationClient and get the classify messages. */
    private void classify(final String text) {
        handler.post(
                () -> {
                    // Run text classification with TF Lite.
                    List<TextClassificationClient.Result> results = client.classify(text);

                    // Show classification result on screen
                    showResult(text, results);
                });
    }

    /** Show classification result on the screen. */
    private void showResult(final String inputText, final List<TextClassificationClient.Result> results) {
        // Run on UI thread
        runOnUiThread(
                () -> {
                    inputEditText.getText().clear();
                    messageList.add(new Message(inputText, "me"));
                    mAdapter.notifyDataSetChanged();
                    final Handler handler = new Handler();
                    //delay reply execution to imitate real chatbot
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageList.add(new Message(createResponse(getTheMostLikely(results).getTitle()), "bot"));
                            mAdapter.notifyDataSetChanged();
                            recyclerView.smoothScrollToPosition(messageList.size()-1);
                        }
                    }, 250);

                });
    }

    //a method that looks for the highest value in the array of results. The highest value is most likely the desired text label
    private TextClassificationClient.Result getTheMostLikely(List<TextClassificationClient.Result> results){
        return Collections.max(results, Comparator.comparingDouble(TextClassificationClient.Result::getConfidence));
    }

    //reply to the user depending on what he/she asked
    private String createResponse(String title) {
        switch (title) {
            case "advice":
                return getString(R.string.chat_advice);
            case "afford":
                if(Double.parseDouble(budget) > Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " Your current budget is " + budget + ". You spent " + spent + " so far. You are "
                            + (Double.parseDouble(budget) - Double.parseDouble(spent)) + " below your budget";
                else if(Double.parseDouble(budget) <= Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " Your current budget is " + budget + ". You spent " + spent + " so far. You are "
                            + ((Double.parseDouble(budget) - Double.parseDouble(spent))*(-1)) + " over your budget";
            case "backup":
                return getString(R.string.chat_backup);
            case "budget_help":
                if(Double.parseDouble(budget) > Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " Your current budget is " + budget + ". You are "
                            + (Double.parseDouble(budget) - Double.parseDouble(spent)) + " below your budget";
                else if(Double.parseDouble(budget) <= Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " Your current budget is " + budget + ". You are "
                            + ((Double.parseDouble(budget) - Double.parseDouble(spent))*(-1)) + " over your budget";
            case "goodbye":
                return getString(R.string.chat_bye);
            case "greeting":
                return getString(R.string.chat_greeting);
            case "internet":
                return getString(R.string.chat_internet);
            case "options":
                return getString(R.string.chat_options);
            case "spent_help":
                if(Double.parseDouble(budget) > Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " You spent " + spent + " so far. You are "
                            + (Double.parseDouble(budget) - Double.parseDouble(spent)) + " below your budget";
                else if(Double.parseDouble(budget) <= Double.parseDouble(spent))
                    return getString(R.string.chat_afford) + " You spent " + spent + ". You are "
                            + ((Double.parseDouble(budget) - Double.parseDouble(spent))*(-1)) + " over your budget";
            case "thanks":
                return getString(R.string.chat_thanks);
            default:
                return getString(R.string.chat_unknown);
        }
    }

}
