package com.ykarakai.cofund.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.ykarakai.cofund.data.LoginRepository;
import com.ykarakai.cofund.R;

import static com.ykarakai.cofund.ui.main.SplashActivity.mAuth;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(final String username, final String password) {

        mAuth.fetchSignInMethodsForEmail(username).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                if(task.isSuccessful()){
                    SignInMethodQueryResult providerResult = task.getResult();
                    //login
                   if(providerResult.getSignInMethods().contains("password")){
                       mAuth.signInWithEmailAndPassword(username, password)
                               .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                   @Override
                                   public void onComplete(@NonNull Task<AuthResult> task) {
                                       if (task.isSuccessful()) {
                                           // Sign in success, update UI with the signed-in user's information
                                           FirebaseUser user = mAuth.getCurrentUser();
                                           loginResult.setValue(new LoginResult(new LoggedInUserView(username,true)));
                                       } else {
                                           // If sign in fails, display a message to the user.
                                           loginResult.setValue(new LoginResult(R.string.login_failed));
                                       }

                                   }
                               });
                   }
                   //sign up
                   else{
                       mAuth.createUserWithEmailAndPassword(username, password)
                               .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                   @Override
                                   public void onComplete(@NonNull Task<AuthResult> task) {
                                       if (task.isSuccessful()) {
                                           // Sign in success, update UI with the signed-in user's information
                                           FirebaseUser user = mAuth.getCurrentUser();
                                           loginResult.setValue(new LoginResult(new LoggedInUserView(username, false)));
                                       } else {
                                           // If sign in fails, display a message to the user.
                                           loginResult.setValue(new LoginResult(R.string.login_failed));

                                       }

                                   }
                               });
                   }

                }
            }
        });
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}
