package com.ykarakai.cofund.ui.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.R;
import com.ykarakai.cofund.database.Category;
import com.ykarakai.cofund.database.Transactions;

import java.util.ArrayList;
import java.util.List;

import static com.ykarakai.cofund.ui.main.ChartFragment.buildChart;

public class Details extends AppCompatActivity {
    private FloatingActionButton addbtn;
    public static String dateName;
    public static ArrayList<String> categories = new ArrayList<String>();
    public static int dateId;
    public static List<Transactions> transacs = new ArrayList<Transactions>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        dateId = intent.getIntExtra("date", 0);
        dateName = intent.getStringExtra("dateName");

        getTransactions();
        getCategories();

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));

        addbtn = findViewById(R.id.add);
        addbtn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));


        this.getSupportActionBar().hide();
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAddDialog();
            }
        });

    }


    private void getTransactions(){
        MainActivity.mViewModel.getTransactionsByDateId(dateId).observe(this, data -> {
            transacs.clear();
            transacs.addAll(data);
            TransactionsFragment.adapter.notifyDataSetChanged();
        });
    }

    private void getCategories(){
        MainActivity.mViewModel.getAllCategories().observe(this, data -> {
            for (int i = 0; i < data.size(); ++i){
                categories.add(data.get(i).getName());
            }
        });
    }

    private void openAddDialog(){
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText name = new EditText(getApplicationContext());
        final EditText amount = new EditText(getApplicationContext());
        amount.setInputType(InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL);

        final Spinner categorySpinner = new Spinner(getApplicationContext());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Details.this, android.R.layout.simple_spinner_dropdown_item, categories);
        categorySpinner.setAdapter(spinnerArrayAdapter);

        layout.addView(name);
        layout.addView(amount);
        layout.addView(categorySpinner);

        AlertDialog.Builder builder = new AlertDialog.Builder(Details.this);

        builder.setTitle(R.string.add_transac_title)
                .setPositiveButton(R.string.add_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        double amountD = 0.0;
                        int categoryI = 0;
                        try{
                            try{
                                amountD = Double.parseDouble(amount.getText().toString());
                                categoryI = (int) categorySpinner.getSelectedItemId() + 1;
                            }catch(Exception ex){
                                Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.error_parsing_snackbar,
                                        Snackbar.LENGTH_SHORT)
                                        .show();
                            }
                            MainActivity.mViewModel.insert(new Transactions(name.getText().toString(), categoryI, dateId, amountD));
                            MainActivity.mViewModel.updateSpent(dateName, amountD);
                            buildChart();
                            Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.inserted_snackbar,
                                    Snackbar.LENGTH_SHORT)
                                    .show();
                        }catch(Exception ex){
                            Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.error_snackbar,
                                    Snackbar.LENGTH_SHORT)
                                    .show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.setView(layout);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(name.getText().toString().length() == 0 || amount.getText().toString().length() == 0){//if (TextUtils.isEmpty(s)) {
                    // Disable add button
                    ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

                } else {
                    ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        name.addTextChangedListener(textWatcher);
        amount.addTextChangedListener(textWatcher);

    }

}