package com.ykarakai.cofund.ui.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.ykarakai.cofund.Adapter;
import com.ykarakai.cofund.MainActivity;
import com.ykarakai.cofund.R;


public class TransactionsFragment extends Fragment {
    public static RecyclerView.Adapter adapter;
    private int dateId;
    private String dateName;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ItemTouchHelper.SimpleCallback touchCallbacl;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static TransactionsFragment newInstance(int index) {
        TransactionsFragment fragment = new TransactionsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
        dateId = getArguments().getInt("dateId");
        dateName = getArguments().getString("dateName");
        swiped();

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_transactions, container, false);


        recyclerView = (RecyclerView) root.findViewById(R.id.transacList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new Adapter(Details.transacs,getContext());
        recyclerView.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(touchCallbacl);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        return root;
    }

    //swipe for delete functionality
    private void swiped(){
        touchCallbacl = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.delete_dialog_name)
                        .setMessage(R.string.delete_dialog_message)

                        .setPositiveButton(R.string.delete_dialog_confirm, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    MainActivity.mViewModel.delete(Details.transacs.get(viewHolder.getAdapterPosition()), dateName);
                                    Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), R.string.inserted_snackbar,
                                            Snackbar.LENGTH_SHORT)
                                            .show();
                                }catch (Exception ex){

                                }
                                adapter.notifyDataSetChanged();
                            }

                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                adapter.notifyDataSetChanged();

                            }
                        })
                        .create().show();
            }
        };
    }

}